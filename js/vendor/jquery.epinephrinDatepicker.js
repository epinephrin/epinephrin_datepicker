;(function($, window, document, undefined) {

    'use strict';

    var pluginName = 'epinephrinDatepicker',
        // значения по умолчанию
        defaults = {
            format: 'dd-mm-yyyy'
        },
        // для placeholder надо дд мм гггг
        Calendar = {
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
                'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
        };

    // конструктор
    function Plugin(element, options) {
        var that = this;
        this.element = $(element);

        // создать календарь в конце страницы
        this.calendar = $(this._generateHtml())
            .appendTo('body')
            .on({
                click: $.proxy(this, 'click')
            });

        // если кликнули не по календорю, то его надо скрыть
        $(document).on('mousedown', function(e) {
            if ($(e.target).closest('.epinephrin-datepicker').length === 0 &&
                !$(e.target).is(that.element)) {
                
                that.hide();
            }
        });

        // разместить календарь ниже поля
        var offset = this.element.offset();
        this.calendar.css({
            top: offset.top + this.element.outerHeight(true),
            left: offset.left
        });

        // нет события paste
        this.element.on({
            focus: $.proxy(this, 'show'),
            keypress: $.proxy(this, 'validate')
        });

        // при многократном вызове функции настройки будут сохранятся, и замещаться при необходимости
        this.options = $.extend({}, defaults, options);

        this.init();
    }

    Plugin.prototype = {
        init: function () {
            // для удобства можно добавить placeholder с вводимым форматом даты
            var formatChars = {
                    'd': 'д',
                    'm': 'м',
                    'y': 'г'
                },
                placeholder = this.options.format.replace(/[dmy]/g, function(s) {
                    return formatChars[s];
                });

            this.element.attr('placeholder', placeholder);

            // выбранная дата
            this.currentDate = this.parseDate(this.element.val());
            // отображаемая дата
            this.viewDate = this.parseDate(this.element.val());

            // заполнить календарь выбранным месяцем
            this._fillCalendar();
        },
        show: function () {
            this.calendar.show();
        },
        hide: function () {
            if (!this.calendar.is(':visible')) return;
            this.calendar.hide();
        },
        click: function (e) {
            var $target = $(e.target);

            // смена месяца
            if ($target.is('span[class^="btn-"]')) {
                var monthValue = 0;

                if ($target.is('span.btn-next')) {
                    // следующий месяц
                    monthValue = 1;
                } else if ($target.is('span.btn-prev')) {
                    // предыдущий месяц
                    monthValue = -1;
                }

                this.viewDate.setMonth(this.viewDate.getMonth() + monthValue);
                this._fillCalendar();
            }

            // смена даты
            // можно добавить листалку по годам
            if ($target.is('td:not(".active, .disabled")')) {
                this.currentDate.setDate($target.text());
                this.currentDate.setMonth(this.viewDate.getMonth());
                this.currentDate.setFullYear(this.viewDate.getFullYear());

                // закрыть
                this.hide();

                this._fillCalendar();

                // вставить выбранную дату в нужном формате
                var formattedDate = this.getFormattedDate(this.currentDate);
                this.element.val(formattedDate);
            }

            // сегодняшняя дата
            if ($target.is('div.current-date span')) {
                // нет проверки что эта дата или месяц уже выбраны, чтобы не выполнять код
                var date = new Date();
                this.currentDate.setDate(date.getDate());
                this.currentDate.setMonth(date.getMonth());
                this.currentDate.setFullYear(date.getFullYear());

                this.viewDate.setDate(date.getDate());
                this.viewDate.setMonth(date.getMonth());
                this.viewDate.setFullYear(date.getFullYear());

                // закрыть
                this.hide();

                this._fillCalendar();

                // вставить выбранную дату в нужном формате
                var formattedDate = this.getFormattedDate(this.currentDate);
                this.element.val(formattedDate);
            }
        },
        // преобразование даты из строки в обьект Date, если дата не указана, то вернуть текущую
        parseDate: function(value) {
            var date = new Date();

            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            date.setMilliseconds(0);

            var delimiter = /[^mdy]/.exec(this.options.format)[0],
                format = this.options.format.split(delimiter),
                dateArray = value.split(delimiter),
                m, d, y;

            for (var i = 0, len = format.length; i < len; i++) {
                if (/m/.test(format[i])) m = dateArray[i];
                if (/d/.test(format[i])) d = dateArray[i];
                if (/y/.test(format[i])) y = dateArray[i];
            }

            // если в поле дата
            if (m > 0 && m < 13 &&
                y && y.length === 4 &&
                d > 0 && d <= (new Date(y, m, 0)).getDate()) {

                date.setDate(d);
                date.setMonth(m - 1);
                date.setFullYear(y);
            }

            return date;
        },
        getFormattedDate: function (date) {
            var day = date.getDate(),
                month = date.getMonth() + 1,
                year = date.getFullYear(),
                delimiter = /[^mdy]/.exec(this.options.format)[0];

            day = this.checkZero(day);
            month = this.checkZero(month);

            // предусмотрены фиксированные варианты, без учета очередности дня, месяца и года
            return (day + delimiter + month + delimiter + year);
        },
        // проверка на разрешенные для ввода символы
        validate: function (e) {
            var event = e || window.event,
                charCode = event.which || event.keyCode,
                charStr = String.fromCharCode(charCode),
                delimiter = /[^mdy]/.exec(this.options.format)[0];

            //console.log(charCode);
            //37 - arrow left
            //39 - arrow right
            //116 - F5, ctrl + F5
            //9 - tab
            //13 - enter
            //8 - backspace
            //46 - del

            var regex = new RegExp("\\d|\\" + delimiter);
            if (!regex.test(charStr) && '37' != charCode && '39' != charCode &&
                '116' != charCode && '13' != charCode && '9' != charCode &&
                '8' != charCode && '46' != charCode) {
                
                event.returnValue = false;
                if (event.preventDefault) {
                    event.preventDefault();
                }
            }
        },
        // количество дней в феврале
        getFebriaryDays: function(year) {
            return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) ? 29 : 28;
        },
        getMonthDays: function(year, month) {
            return ['31', this.getFebriaryDays(year), '31', '30', '31', '30', '31', '31', '30', '31', '30', '31'][month]
        },
        // добавляет 0 перед днем или месяцем при однозначном числе
        checkZero: function(value) {
            return value < 10 ? '0' + value : value;
        },
        _generateHtml: function () {
            // дни недели начинаются только с понедельника
            var html = '<div class="epinephrin-datepicker">' +
                           '<div class="epinephrin-datepicker-header">' +
                               '<span class="btn-prev">&lt;</span>' +
                               '<span class="btn-next">&gt;</span>' +
                               '<div class="month"></div>' +
                           '</div>' +
                           '<table>' +
                               '<thead>' +
                                   '<tr>' +
                                       '<th>' + Calendar.daysShort[1] + '</th>' +
                                       '<th>' + Calendar.daysShort[2] + '</th>' +
                                       '<th>' + Calendar.daysShort[3] + '</th>' +
                                       '<th>' + Calendar.daysShort[4] + '</th>' +
                                       '<th>' + Calendar.daysShort[5] + '</th>' +
                                       '<th>' + Calendar.daysShort[6] + '</th>' +
                                       '<th>' + Calendar.daysShort[0] + '</th>' +
                                   '</tr>' +
                               '</thead>' +
                               '<tbody>' +
                                   '<tr>' +
                                       '<td colspan="7"></td>' +
                                   '</tr>' +
                               '</tbody>' +
                           '</table>' +
                           '<div class="current-date"><span>Сегодня</span></div>' +
                       '</div>';
            return html;
        },
        // заполняет календарь
        _fillCalendar: function () {
            var month = this.viewDate.getMonth() + 1,
                year = this.viewDate.getFullYear(),
                firstDayDate = new Date(month + ' 1, ' + year),
                monthDays = this.getMonthDays(year, this.viewDate.getMonth()),
                lastDayDate = new Date(month + ' ' + monthDays + ', ' + year),
                // день недели с которого начинается отображаемый месяц
                firstDayWeekday = firstDayDate.getDay(),
                // день недели которым заканчивается отображаемый месяц
                lastDayWeekday = lastDayDate.getDay(),
                // для выделения выбранной даты
                activeDay = this.currentDate.getDate(),
                activeMonth = this.currentDate.getMonth() + 1,
                activeYear = this.currentDate.getFullYear(),
                // пустая ячейка таблицы
                emptyCell = '<td class="disabled"></td>',
                html = '<tr>';

            // для удобства воскресенью делаю индекс 7
            if (0 == firstDayWeekday) firstDayWeekday = 7;
            if (0 == lastDayWeekday) lastDayWeekday = 7;

            // ячейки с днями предыдущего месяца
            for (var i = 1; i < firstDayWeekday; i++) {
                html += emptyCell;
            }

            // ячейки с днями выбранного месяца
            for (var day = 1, weekday = firstDayWeekday; day <= monthDays; day++, weekday++) {
                if (weekday > 7 ) {
                    weekday = 1;
                    html += '</tr><tr>';
                }

                html += '<td';
                // выбранная дата
                if (day == activeDay && month == activeMonth && year == activeYear) {
                    html += (day == activeDay ? ' class="active"' : '');
                }
                html += '>' + day + '</td>';
            }

            // ячейки с днями следующего месяца
            while (lastDayWeekday < 7) {
                html += emptyCell;
                lastDayWeekday++;
            }

            html += '</tr>';

            $('tbody', this.calendar).empty().append(html);
            $('div.month', this.calendar).empty().html(Calendar.months[month - 1] + ' ' + year);
        }
    };

    $.fn[pluginName] = function(options) {
        // функционал
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
    
})(jQuery, window, document);